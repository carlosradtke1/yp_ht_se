=============
Carlos Radtke
=============
:author: Carlos Morsch Radtke
:date: 3 Dec 2020


.. note:: 
   There are some useful things I wanted to save,
   but most of them is to exercise the use of **sphinx**.


.. toctree::
   :glob:
   :maxdepth: 2

   */index
