===================
Useful git commands
===================
:author: Carlos Morsch Radtke
:date: 4 Dec 2020

Pull the changes from a team mate's fork
-----------------------------------------------

.. code-block:: console

   git remote add <friend-name> <http-path-to-friends-fork>
   git pull <friend-name> master

Change username and email on git config
---------------------------------------

.. code-block:: console
   
   git config --global user.name "username"
   git config --global user.email "username@email.com"


.. toctree::
   :glob:
   :maxdepth: 1