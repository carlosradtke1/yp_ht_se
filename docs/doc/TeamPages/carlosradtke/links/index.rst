============
Useful Links
============
:author: Carlos Morsch Radtke
:date: 4 Dec 2020

Here are the wonderful list of links:

 `Build with Sphinx <https://pathways-extensions-training.readthedocs.io/en/latest/training/gettingStarted/Sphinx.html#documentatie-builden>`_ |brk|
 `Gatekeeper approach on Git <https://dds-demonstrators.readthedocs.io/en/latest/Teams/1.Hurricane/gitStructure.html>`_ |brk|
 `Git: how to merge repos <https://dds-demonstrators.readthedocs.io/en/latest/Teams/1.Hurricane/merging_git_repos.html>`_ |brk|
 `Git: distributed workflow <https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows>`_ |brk|

   


.. toctree::
   :glob:
   :maxdepth: 1

.. # define a hard line break for HTML
.. |brk| raw:: html

   <br />